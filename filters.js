$(document)
		.ready(
				function() {
					var scope = angular.element($('#filtringApp')).scope();

					scope.doSearch = function(carac, value, type) {
						if (carac !== null && value !== null && type !== null) {
							var filter = {
								caracs : carac,
								values : [ value ],
								t : type
							};
							var index = filterExists(scope.filtringMap, filter);
							if (index == -1) {
								scope.filtringMap.push(filter);

								if (value === 'Structured Funds') {
									scope.structured = true;
								}
							} else {
								if (value === 'Structured Funds') {
									scope.structured = false;
								}
								if (type == 'compare') {
									scope.filtringMap[index].values = [ value ];
								} else {
									var exists = false;
									for (var i = 0; i < scope.filtringMap[index].values.length; i++) {
										if (scope.filtringMap[index].values[i] == value) {
											exists = true;
											break;
										}
									}
									if (exists) {
										var indexVal = scope.filtringMap[index].values
												.indexOf(value);
										if (index > -1) {
											scope.filtringMap[index].values
													.splice(indexVal, 1);
											if (scope.filtringMap[index].values.length === 0) {
												scope.filtringMap.splice(index,
														1);
											}
										}
									} else {
										scope.filtringMap[index].values
												.push(value);
									}

								}
							}
						}

						scope.filtredData = [];
						var imIn = scope.filtringMap.length > 0;
						if (imIn) {
							if (scope.structured) {
								toBeFiltred = clone(scope.structuredData);
							} else {
								toBeFiltred = clone(scope.initialData);
							}
							angular.forEach(toBeFiltred, function(fund) {
								var fundShares = [];
								angular.forEach(fund.shareClassDTOs, function(
										share) {
									var test = oneKeyIsFalse(scope.filtringMap,
											share.filteringCharacteristics);
									if (test) {
										if (fundShares.indexOf(share) == -1) {
											fundShares.push(share);
										}
									}

								});
								if (fundShares.length > 0) {
									fund.shareClassDTOs = fundShares;
									scope.filtredData.push(fund);
								}
							});
						}

						else {
							scope.filtringMap = [];
							structured = false;
							scope.filtredData = clone(scope.initialData);
							toBeFiltred = clone(scope.initialData);
						}
						scope.tableParams.reload();
					}
					scope.initData = function() {
						scope.filtringMap = [];
						structured = false;
						scope.filtredData = clone(scope.initialData);
						toBeFiltred = clone(scope.initialData);
						scope.tableParams.reload();

					}
					scope.deleteOldCarac = function(newCarc, oldCarac) {
						var index = -1;
						var oldFilter;
						for (var i = 0; i < scope.filtringMap.length; i++) {
							if (scope.filtringMap[i].caracs == oldCarac) {
								index = i;
								oldFilter = scope.filtringMap[i];
								break;
							}
						}
						if (index !== -1) {
							scope.filtringMap.splice(index, 1);
							scope.doSearch(newCarc, oldFilter.values[0],
									oldFilter.t);
						}
					}
					scope.removeFilterCarac = function(filter, value) {
						if(value==='Structured Funds'){
							scope.structured=false;
						}
						var index = filterExists(scope.filtringMap, filter);

						if (index !== -1) {
							var valIndex = scope.filtringMap[index].values
									.indexOf(value);
							if (valIndex !== -1) {
								scope.filtringMap[index].values.splice(
										valIndex, 1);
								if (!scope.filtringMap[index].values.length > 0) {
									scope.filtringMap.splice(index, 1);
								}
							}
							scope.doSearch(null, null, null);
						}
					}
					scope.toggleClass = function(assetClass) {
						if (isString(assetClass)) {
							var replaced = assetClass.split(' ').join('-');
							var str = '.dashboard-stat.'.concat(replaced);
							toggle(str);
						}
					}
					scope.checkUncheck = function(assetClass) {
						if (isString(assetClass)) {
							var selector = '.checkBoxFilter.'
									.concat(assetClass);
							checkUncheck(selector);
						}
					}
					scope.filterWeight = function(carac, value) {
						var shares = [];
						angular
								.forEach(
										scope.filtredData,
										function(fund) {
											angular
													.forEach(
															fund.shareClassDTOs,
															function(share) {
																var test = share.filteringCharacteristics[carac] == value;
																if (test) {
																	if (shares
																			.indexOf(share) == -1) {
																		shares
																				.push(share);
																	}
																}

															});
										});
						return shares.length;
					}
					scope.isString = function(value) {
						return isString(value);
					}
				});