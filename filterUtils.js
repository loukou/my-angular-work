function oneValIsTrue(filter, criteria) {
	for (var i = 0; i < filter.values.length; i++) {
		if (filter.t === 'exact') {
			if (criteria == filter.values[i]) {
				return true;
			}
		} else if (filter.t === 'compare') {
			if (criteria !== '' && criteria > filter.values[i]) {
				return true;
			}
		}
	}
	return false;
}

function oneKeyIsFalse(filters, criteria) {
	for (var i = 0; i < filters.length; i++) {
		if (!oneValIsTrue(filters[i], criteria[filters[i].caracs])) {
			return false;
		}
	}
	return true;
}

function filterExists(filters, filter) {
	for (var i = 0; i < filters.length; i++) {
		if (filters[i].caracs == filter.caracs) {
			return i;
		}
	}
	return -1;

}
function toggle(str) {
	$(str).toggleClass('active-filter');
	$(str).parent().find('.circle').toggleClass('white');
}
function uncheckAll() {
	$('.checkBoxFilter').prop('checked', false);
}
function checkUncheck(selector) {
	$(selector).prop('checked', !$(selector).is(':checked'));
}
function isString(s) {
	return typeof (s) === 'string' || s instanceof String;
}
function clone(obj) {
	var copy;

	// Handle the 3 simple types, and null or undefined
	if (null == obj || "object" != typeof obj) {
		if (isString(obj)) {
			if (!jQuery.isArray(obj) && (obj - parseFloat(obj) + 1) >= 0) {
				return Number(obj);
			}
		}
		return obj;
	}
	// Handle Date
	if (obj instanceof Date) {
		copy = new Date();
		copy.setTime(obj.getTime());
		return copy;
	}

	// Handle Array
	if (obj instanceof Array) {
		copy = [];
		for (var i = 0, len = obj.length; i < len; i++) {
			copy[i] = clone(obj[i]);
		}
		return copy;
	}

	// Handle Object
	if (obj instanceof Object) {
		copy = {};
		for ( var attr in obj) {
			if (obj.hasOwnProperty(attr))
				copy[attr] = clone(obj[attr]);
		}
		return copy;
	}

	throw new Error("Unable to copy obj! Its type isn't supported.");
}

$(document).ready(function() {

	$('.dropdown-menu').click(function(event) {
		event.stopPropagation();
	});

	$(".cd-filter-trigger").click(function() {
		$(".filter-opened").animate({
			left : "0"
		}, 500);
		$('.ssm-overlay').show();
	});

	$(".ssm-overlay").click(function() {
		$(".filter-opened").animate({
			left : "-320px"
		}, 500);
		$('.ssm-overlay').hide();
	});
	$(".close-filter-responsive").click(function() {
		$(".filter-opened").animate({
			left : "-320px"
		}, 500);
		$('.ssm-overlay').hide();
	});

});

$(window).on(
		'resize',
		function() {
			$('.advanced-filters-accordion ').toggleClass('filter-opened',
					$(window).width() <= 991);
			$('.gomet-filters-accordion').toggleClass('filter-hidden',
					$(window).width() <= 991);

			if ($(window).width() > 991) {
				$(".ssm-overlay").css('display', 'none');
				$(".advanced-filters-accordion").css('left', '-320px');
			}

		}).trigger('resize');